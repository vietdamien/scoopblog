# ScoopBlog
This is the server-side web programming project that Raphaël Rouseyrol and I, carried out during our second year of *DUT Informatique*. <br>
This is a blog where administrators can create posts and everyone can comment these posts using a pseudonym. Administrators can also manage users (delete existing users, or promote them to the rank of administrator).<br>
A preview is available [here](http://damiennguyen.com/ScoopBlog/Application/).

## Authors
* **[Damien Nguyen](https://gitlab.com/DamsNg63)**
* **[Raphaël Rouseyrol](https://gitlab.iut-clermont.uca.fr/rarouseyro)**

## Installation

### Requirements
 - Apache2
 - MySQL 5.7.24
 - PHP 7.1 or above

### Database setup
You need to create a database named **ScoopBlog** and give the user of your choice and give them data, structure and administration privileges. Once the user is created, you may edit these lines in ```Application/config/config.php```:
```php
/* ========== DATABASE CREDENTIALS ========== */
$dsn        = 'mysql:host=<host>; dbname=ScoopBlog; charset=utf8mb4';
$dbUsername = '<username>';
$dbPassword = '<password>';
```
*If you decide to import ```ScoopBlog.sql``` script, you will have posts in it. The credentials to test the actions are as follows (username => password):*

```
salva  => SALVAtest12*
lambda => lambda*
```

## Additional features

 - [Bootstrap4](https://getbootstrap.com)
 - [TinyMCE](https://www.tiny.cloud)

