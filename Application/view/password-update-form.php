<?php
/* *********************************************************************************************************************
 * Project name: Application
 * FIle name   : password-update-form.php
 * Author      : Damien Nguyen
 * Date        : Friday, December 21 2018 
 * ********************************************************************************************************************/

global $content;
?>
<!DOCTYPE html>
<html lang='en_US'>

<?php include(ROOT . $content['head']) ?>

<body>
    <div class='container align-middle'>
        <div class='row h-100 align-items-center'>
            <div class='col-md-3'></div>
            <div class='col-md-6'>
                <div class='card p-4 bg-dark slightly-transparent'>
                    <div class='card-header'>
                        <h2 class='card-title text-center text-success'>Password change</h2>
                    </div>
                    <div class='card-body'>
                        <form class='mt-1 mb-2' method='post' action='?action=updatePassword'>
                            <?php
                            if (isset($warning)) {
                                echo '<p class=\'font-weight-bold text-center text-danger\'>' . $warning . '</p>';
                            }
                            ?>

                            <div class='form-group'>
                                <label for='currentPassword'>Current password</label>
                                <input class='bg-dark form-control text-light' id='currentPassword'
                                       name='currentPassword' type='password' autofocus required>
                            </div>

                            <div class='form-group'>
                                <label for='newPassword'>New password</label>
                                <input class='bg-dark form-control text-light' id='newPassword' name='newPassword'
                                       type='password' required>
                            </div>

                            <div class='form-group'>
                                <label for='newPasswordC'>New password confirmation</label>
                                <input class='bg-dark form-control text-light' id='newPasswordC' name='newPasswordC'
                                       type='password' required>
                            </div>

                            <button class='btn btn-success btn-md btn-block mt-5' type='submit'
                                    name='register'>Submit
                            </button>

                        </form>
                        <button class='btn btn-outline-secondary btn-md btn-block mt-2 mb-1'
                                onclick='window.location.href="?"'>Cancel
                        </button>
                    </div>
                </div>
            </div>
            <div class='col-md-3'></div>
        </div>
    </div>
</body>

</html>
