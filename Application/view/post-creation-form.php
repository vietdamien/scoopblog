<?php
/* *********************************************************************************************************************
 * Project name: Application
 * FIle name   : post-creation-form.php
 * Author      : Damien Nguyen
 * Date        : Friday, December 14 2018 
 * ********************************************************************************************************************/

global $content;
?>
<!DOCTYPE html>
<html lang='en_US'>

<?php include($content['head']) ?>

<body>
    <nav class='bg-dark mb-4 navbar navbar-expand-lg navbar-dark'>
        <a class='font-lobster navbar-brand'>Post detail</a>
        <button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarSupportedContent'
                aria-controls='navbarSupportedContent' aria-expanded='false' aria-label='Toggle navigation'>
            <span class='navbar-toggler-icon'></span>
        </button>

        <div class='collapse navbar-collapse' id='navbarSupportedContent'>
            <ul class='mr-auto navbar-nav'>
                <li class='nav-item'>
                    <a class='nav-link' href='?'><i class='fa fa-users' aria-hidden='true'></i> Posts list</a>
                </li>
                <?php include(ROOT . $content['connection-state-buttons']) ?>

            </ul>
        </div>
    </nav>

    <div class='container-fluid'>
        <table class='mb-4 slightly-transparent table table-bordered table-dark table-striped text-justify'>
            <tr>
                <th>
                    <h1 class='text-center'>Post creation</h1>
                </th>
            </tr>
            <tr>
                <td colspan='8'>
                    <form class='text-center' method='post' action='?action=createPost'>
                        <?php
                        if (isset($warning)) {
                            echo '<p class=\'font-weight-bold text-center text-danger\'>' . $warning . '</p>';
                        }
                        ?>

                        <input class='bg-dark border-secondary form-control mb-1 text-light' name='title'
                               placeholder='Title' required>
                        <textarea class='bg-dark border-secondary form-control mb-1 text-light' id='content'
                                  name='content' placeholder='Your post here...' rows='10'></textarea>
                        <button class='btn btn-outline-success' type='submit'>Submit</button>
                    </form>
                </td>
            </tr>
        </table>
    </div>
</body>

</html>
