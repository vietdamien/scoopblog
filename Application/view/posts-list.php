<?php
/* *********************************************************************************************************************
 * Project name: Application
 * FIle name   : posts-list.php
 * Author      : Damien Nguyen
 * Date        : Thursday, November 29 2018
 * ********************************************************************************************************************/

global $content, $isAdministrator;
?>
<!DOCTYPE html>
<html lang='en_US'>

<?php include(ROOT . $content['head']) ?>

<body>
    <nav class='bg-dark mb-4 navbar navbar-expand-lg navbar-dark'>
        <a class='font-lobster navbar-brand'>Posts</a>
        <button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarSupportedContent'
                aria-controls='navbarSupportedContent' aria-expanded='false' aria-label='Toggle navigation'>
            <span class='navbar-toggler-icon'></span>
        </button>

        <div class='collapse navbar-collapse' id='navbarSupportedContent'>
            <ul class='mr-auto navbar-nav'>
                <li class='nav-item'>
                    <a class='active nav-link'><i class='fa fa-users' aria-hidden='true'></i> Posts list</a>
                </li>
                <li class='nav-item dropdown'>
                    <a class='nav-link dropdown-toggle' href='#' id='navbarDropdown' role='button'
                       data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                        Information
                    </a>
                    <div class='dropdown-menu bg-dark' aria-labelledby='navbarDropdown'>
                        <a class='dropdown-item text-secondary'>
                            Local messages: <?= $cookieCommentsCounter ?? DEFAULT_INT_VALUE ?>

                        </a><?php if ($isAdministrator) { ?>

                        <a class='dropdown-item text-secondary'>
                            Total messages: <?= $totalCommentsCounter ?? DEFAULT_INT_VALUE ?>

                        </a><?php } ?>

                    </div>
                </li>
                <?php include(ROOT . $content['connection-state-buttons']) ?>

            </ul>

            <ul class='navbar-nav'>
                <li>
                    <form class='form-inline mr-2 my-2 my-lg-0' method='post' action='?action=updatePageSize'>
                        <input class='bg-dark border-secondary form-control mr-sm-2 text-light' name='pageSize'
                               type='number' min='1' max='50' aria-label='Page size'
                               placeholder='<?= DEFAULT_PAGE_SIZE ?>'>
                        <button class='btn btn-outline-success my-2 my-sm-0' data-toggle='tooltip'
                                data-placement='bottom' title='Update the number of posts per page.' type='submit'>
                            <i class='fa fa-sync-alt text-success'></i> Update page size
                        </button>
                    </form>
                </li>
                <li>
                    <form class='form-inline my-2 my-lg-0' method='post' action='?'>
                        <input class='bg-dark border-secondary form-control mr-sm-2 text-secondary' id='searchInput'
                               name='search' placeholder='Search' aria-label='Search' type='date'>
                        <select class='bg-dark border-secondary custom-select form-control mr-sm-2 px-2 text-secondary'
                                aria-label='SearchSelect' id='searchSelect' name='type'
                                onchange='onSearchSelectionChanged()'>
                            <option value='<?= DATE ?>'>Date</option>
                            <option value='<?= KEY_WORD ?>'>Key word</option>
                        </select>
                        <button class='btn btn-outline-success my-2 my-sm-0' data-toggle='tooltip'
                                data-placement='bottom' title='Search by title, content or author.' type='submit'>
                            <i class='fa fa-search text-success'></i> Search
                        </button>
                    </form>
                </li>
            </ul>
        </div>
    </nav>

    <div class='container-fluid '>
        <?php if (isset($warning)) { ?><p
                class='font-weight-bold text-center text-danger'><?= $warning ?></p> <?php } ?>

        <table class='mb-4 slightly-transparent table table-bordered table-dark table-striped text-justify'
               name='responsiveTable'><?php if (!isset($posts) || empty($posts)) { ?>

                <tr>
                    <th colspan='8'>
                        <p class='font-weight-bold text-center text-danger'>The posts list is empty!</p>
                    </th>
                </tr><?php } else { ?>

                <tr>
                <th>Author</th>
                <th class='max-sized'>Date</th>
                <th>Title</th><?php if ($isAdministrator) { ?>

                    <th class='max-sized text-center'>
                        <button class='btn btn-outline-danger' onclick='confirmDeletion()'>
                            <i class='far fa-trash-alt text-danger'></i>
                        </button>
                    </th><?php } ?>

                </tr><?php foreach ($posts as $post) { ?>

                    <tr>
                    <td>
                        <a class='text-danger' href='?action=displayUserDetail&username=<?= $post->getAuthor() ?>'>
                            <?= $post->getAuthor() ?>
                        </a>
                    </td>
                    <td class='max-sized'>
                        <p><?= $post->getDate() ?></p>
                    </td>
                    <td>
                    <a class='text-success' href='?action=displayPostDetail&id=<?= $post->getId() ?>'>
                        <?= $post->getTitle() ?>
                    </a>
                    </td><?php if ($isAdministrator) { ?>

                        <td class='max-sized'>
                        <form class='text-center' method='post' action='?action=deletePosts'>
                            <input type='hidden' name='id' value='<?= $post->getId() ?>'>
                            <button class='btn btn-outline-danger' type='submit'>
                                <i class='far fa-trash-alt text-danger'></i>
                            </button>
                        </form>
                        </td><?php } ?>

                    </tr><?php
                }
            } ?>

        </table><?php if (($currentPage = $currentPage ?? DEFAULT_INT_VALUE) > DEFAULT_INT_VALUE) { ?>

            <nav aria-label='...'>
            <ul class='justify-content-center pagination pagination-lg'>
                <li class='page-item'><a class='bg-dark border-secondary page-link text-light' href='?page=1'>1</a></li>
                <li class='page-item'>
                    <a class='bg-dark border-secondary page-link'
                       href='?page=<?= ($currentPage - 1 > 1 ? $currentPage - 1 : MINIMUM_PAGE_VALUE) ?>' aria-label='
                       Previous'>
                        <span aria-hidden='true'>&laquo;</span>
                        <span class='sr-only'>Previous</span>
                    </a>
                </li>
                <li class='page-item'><a class='bg-dark border-secondary page-link'><?= $currentPage ?></a></li>
                <li class='page-item'>
                    <a class='bg-dark border-secondary page-link'
                       href='?page=<?= ($currentPage + 1 < ($numberOfPages = $numberOfPages ?? MINIMUM_PAGE_VALUE)
                               ? $currentPage + 1
                               : $numberOfPages) ?>'
                       aria-label='Next'>
                        <span aria-hidden='true'>&raquo;</span>
                        <span class='sr-only'>Next</span>
                    </a>
                </li>
                <li class='page-item'>
                    <a class='bg-dark border-secondary page-link text-light'
                       href='?page=<?= $numberOfPages ?>'><?= $numberOfPages ?></a>
                </li>
            </ul>
            </nav><?php } ?>

    </div>
</body>

<script type="text/javascript">
    function confirmDeletion() {
        confirm("Are you sure you want to delete everything?") ? window.location.href = "?action=deletePosts" : null;
    }

    function onSearchSelectionChanged() {
        const searchSelect = document.getElementById('searchSelect');
        $('#searchInput').prop('type', searchSelect[searchSelect.selectedIndex].value === '<?= DATE ?>' ? 'date' : 'search');
    }
</script>

</html>
