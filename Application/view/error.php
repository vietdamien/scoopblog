<?php
/* *********************************************************************************************************************
 * Project name: Application
 * FIle name   : error.php
 * Author      : Damien Nguyen
 * Date        : Tuesday, November 22 2018
 * ********************************************************************************************************************/

global $content, $front;
?>
<!DOCTYPE html>
<html lang='en_US'>

<?php include(ROOT . $content['head']) ?>

<body>
    <div class='align-middle container text-center'>
        <div class='align-items-center h-100 row'>
            <div class='col-md-3'></div>
            <div class='col-md-6'>
                <div class='bg-dark card p-4 slightly-transparent'>
                    <div class='card-header'>
                        <h1 class='text-danger'>Error</h1>
                    </div>
                    <div class='card-body'>
                        <?php if (isset($errors)) {
                            foreach ($errors as $value) {
                                echo '<p class=\'font-weight-bold\'>' . $value . '</p>';
                            }
                        } ?>

                        <button class='btn btn-block btn-outline-secondary' onclick='window.location.href="?"'>
                            Index
                        </button>
                    </div>
                </div>
            </div>
            <div class='col-md-3'></div>
        </div>
    </div>
</body>

</html>
