<?php
/* *********************************************************************************************************************
 * Project name: Application
 * FIle name   : registration-form.php
 * Author      : Damien Nguyen
 * Date        : Friday, November 23 2018
 * ********************************************************************************************************************/

global $content;
?>
<!DOCTYPE html>
<html lang='en_US'>

<?php include(ROOT . $content['head']) ?>

<body>
    <div class='container align-middle'>
        <div class='row h-100 align-items-center'>
            <div class='col-md-3'></div>
            <div class='col-md-6'>
                <div class='card p-2 bg-dark slightly-transparent'>
                    <div class='card-header'>
                        <h2 class='card-title text-center text-success'>Registration</h2>
                    </div>
                    <div class='card-body'>
                        <form class='mt-1 mb-2' method='post' action='?action=register'>
                            <?php
                            if (isset($warning)) {
                                echo '<p class=\'font-weight-bold text-center text-danger\'>' . $warning . '</p>';
                            }
                            ?>

                            <div class='form-group'>
                                <label for='username'>Username</label>
                                <input class='bg-dark form-control text-light' id='username' name='username' autofocus
                                       required>
                            </div>

                            <div class='form-group'>
                                <label for='password'>Password</label>
                                <input class='bg-dark form-control text-light' id='password' type='password'
                                       name='password' required>
                            </div>

                            <div class='form-group'>
                                <label for='passwordC'>Confirm password</label>
                                <input title='password' class='bg-dark form-control text-light' id='passwordC'
                                       type='password' name='passwordC' required>
                            </div>

                            <div class='form-group'>
                                <label for='email'>Email</label>
                                <input class='bg-dark form-control text-light' id='email' type='email' name='email'>
                            </div>

                            <button class='btn btn-success btn-md btn-block mt-5' type='submit'
                                    name='register'>Registration
                            </button>
                            
                        </form>
                        <button class='btn btn-outline-secondary btn-md btn-block mt-2 mb-1'
                                onclick='window.location.href="?action=displayConnectionForm"'>Cancel
                        </button>
                    </div>
                </div>
            </div>
            <div class='col-md-3'></div>
        </div>
    </div>
</body>

</html>
