<?php
/* *********************************************************************************************************************
 * Project name: Application
 * FIle name   : password-recovery-form.php
 * Author      : Damien Nguyen
 * Date        : Friday, December 28 2018 
 * ********************************************************************************************************************/

global $content;
?>
<!DOCTYPE html>
<html lang='en_US'>

<?php include(ROOT . $content['head']) ?>

<body>
    <div class='align-middle container'>
        <div class='align-items-center h-100 row'>
            <div class='col-md-3'></div>
            <div class='col-md-6'>
                <div class='bg-dark card p-2 slightly-transparent'>
                    <div class='card-header'>
                        <h2 class='text-center text-success'>Password recovery</h2>
                    </div>
                    <div class='card-body'>
                        <form class='mt-1 mb-2' method='POST' action='?action=recoverPassword'>
                            <?php if (isset($warning)) {
                                echo '<p class=\'font-weight-bold text-center text-danger\'>' . $warning . '</p>';
                            } ?>

                            <div class='form-group'>
                                <label for='email'><i class='fa fa-at'></i> Email</label>
                                <input class='bg-dark form-control text-light' id='email' name='email' type='email'
                                       autofocus required>
                            </div>
                            <button class='btn btn-block btn-md btn-success mt-5' type='submit' name='connect'>
                                Submit
                            </button>
                        </form>
                        <button class='btn btn-block btn-md btn-primary' type='submit'
                                onclick='window.location.href="?action=displayConnectionForm"'>
                            Connection
                        </button>
                        <button class='btn btn-block btn-md btn-outline-secondary' type='button'
                                onclick='window.location.href="?"'>
                            Cancel
                        </button>
                    </div>
                </div>
            </div>
            <div class='col-md-3'></div>
        </div>
    </div>
</body>

</html>
