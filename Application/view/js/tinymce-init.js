tinymce.init({
    forced_root_block: false,
    plugins: 'code, codesample, colorpicker, hr, image, link, media, preview, table, textcolor',
    selector: 'textarea',
    skin: 'dark',
    toolbar: 'undo redo | styleselect | fontselect | fontsizeselect | bold italic underline | alignleft aligncenter ' +
        'alignright alignjustify | bullist numlist outdent indent | table | hr | forecolor backcolor | code | ' +
        'codesample'
});
