<?php
/* *********************************************************************************************************************
 * Project name: Application
 * FIle name   : users-list.php
 * Author      : Damien Nguyen
 * Date        : Tuesday, December 25 2018 
 * ********************************************************************************************************************/

global $content, $isAdministrator, $roles;
?>
<!DOCTYPE html>
<html lang='en_US'>

<?php
include(ROOT . $content['head']);
if (isset($users)) { ?>

    <body>
        <nav class='bg-dark mb-4 navbar navbar-expand-lg navbar-dark'>
            <a class='font-lobster navbar-brand'>Users</a>
            <button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarSupportedContent'
                    aria-controls='navbarSupportedContent' aria-expanded='false' aria-label='Toggle navigation'>
                <span class='navbar-toggler-icon'></span>
            </button>

            <div class='collapse navbar-collapse' id='navbarSupportedContent'>
                <ul class='mr-auto navbar-nav'>
                    <li class='nav-item'>
                        <a class='nav-link' href='?'><i class='fa fa-users' aria-hidden='true'></i> Posts list</a>
                    </li>
                    <?php include(ROOT . $content['connection-state-buttons']) ?>

                </ul>

                <ul class='navbar-nav'>
                    <li>
                        <form class='form-inline my-2 my-lg-0' method='post' action='?action=displayUsersList'>
                            <input class='bg-dark border-secondary form-control mr-sm-2 text-light' name='search'
                                   type='search' placeholder='Search' aria-label='Search'>
                            <button class='btn btn-outline-success my-2 my-sm-0' data-toggle='tooltip'
                                    data-placement='bottom' title='Search a specific username pattern' type='submit'>
                                <i class='fa fa-search text-success'></i> Search
                            </button>
                        </form>
                    </li>
                </ul>
            </div>
        </nav>

        <div class='container-fluid '><?php if (isset($warning)) {
                echo '<p class=\'font-weight-bold text-center text-danger\'>' . $warning . '</p>';
            } elseif (isset($success)) {
                echo '<p class=\'font-weight-bold text-center text-success\'>' . $success . '</p>';
            }
            ?>

            <table class='mb-4 slightly-transparent table table-bordered table-dark table-striped text-justify'
                   name='responsiveTable'><?php if (!isset($users) || empty($users)) { ?>

                    <tr>
                        <th colspan='8'>
                            <p class='font-weight-bold text-center text-danger'>The users list is empty!</p>
                        </th>
                    </tr><?php } else { ?>

                    <tr>
                    <th>Username</th>
                    <th>Email address</th>
                    <th>Role</th>

                    <?php if ($isAdministrator) { ?>

                        <th class='max-sized text-center'>
                            <button class='btn btn-outline-danger disabled'>
                                <i class='far fa-trash-alt text-danger'></i>
                            </button>
                        </th><?php } ?>

                    </tr><?php $count = 0;
                    foreach ($users as $user) {
                        ++$count; ?>

                        <tr>
                        <td>
                            <a class='text-danger'
                               href='?action=displayUserDetail&username=<?= $user->getUsername() ?>'>
                                <?= $user->getusername() ?>
                            </a>
                        </td>
                        <td>
                            <a href='mailto:<?= $user->getEmail() ?>'><?= $user->getEmail() ?></a>
                        </td>
                        <td><?php if (!(isset($currentSessionUsername)
                                        && ($isCurrentAccount = $currentSessionUsername == $user->getUsername()))) { ?>

                            <form class='form-inline my-2 my-lg-0' method='post' action='?action=updateRole'>
                            <input type='hidden' name='username' value='<?= $user->getUsername() ?>'>
                            <select class='bg-dark custom-select form-control mx-1 px-2 text-light'
                                    id='select<?= $count ?>'
                                    onchange='onSelectionChanged(<?= $count ?>);' name='role'><?php foreach ($roles as
                                                                                                             $key =>
                                                                                                             $value) {
                                    if ($user->getRole() == $value) { ?>

                                        <option value='<?= $value ?>'
                                                selected='selected'><?= $value ?></option><?php } else { ?>

                                        <option value='<?= $value ?>'><?= $value ?></option><?php }
                                } ?>

                            </select>
                            <button class='btn btn-outline-success mx-1 my-2 my-sm-0' data-toggle='tooltip'
                                    data-placement='bottom' disabled id='button<?= $count ?>'
                                    title='Update this user&apos;s role.' type='submit'>
                                <i class='fa fa-sync-alt text-success'></i> Update
                            </button>
                            </form><?php } ?>

                        </td><?php if ($isAdministrator) { ?>

                            <td class='max-sized'><?php if (!$isCurrentAccount) { ?>

                                <form class='text-center' method='post' action='?action=deleteUser'>
                                <input type='hidden' name='username' value='<?= $user->getUsername() ?>'>
                                <button class='btn btn-outline-danger' type='submit'>
                                    <i class='far fa-trash-alt text-danger'></i>
                                </button>
                                </form><?php } ?>

                            </td><?php } ?>

                        </tr><?php }
                } ?>

            </table>
        </div>
    </body>

    <script type="text/javascript">
        function onSelectionChanged(count) {
            const selectIndex = document.getElementById("select" + count).selectedIndex;
            const option = document.getElementsByTagName("option");
            $("#button" + count).prop("disabled", option[selectIndex].defaultSelected);
        }
    </script>

    </html>

    <?php }
