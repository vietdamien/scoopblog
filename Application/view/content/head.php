<?php
/* *********************************************************************************************************************
 * Project name: Application
 * FIle name   : head.php
 * Author      : Damien Nguyen
 * Date        : Tuesday, November 20 2018
 * ********************************************************************************************************************/

global $front;
?>

<!----------------------------------------------------
   _____                       ____  _
  / ____|                     |  _ \| |
 | (___   ___ ___   ___  _ __ | |_) | | ___   __ _
  \___ \ / __/ _ \ / _ \| '_ \|  _ <| |/ _ \ / _` |
  ____) | (_| (_) | (_) | |_) | |_) | | (_) | (_| |
 |_____/ \___\___/ \___/| .__/|____/|_|\___/ \__, |
                        | |                   __/ |
                        |_|                  |___/
----------------------------------------------------->

<head>
    <meta charset='utf-8'>
    <meta name='authors' content='Damien NGUYEN, Raphael ROUSEYROL'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>

    <title>ScoopBlog</title>

    <link rel='shortcut icon' href='<?= $front['favicon'] ?>'>

    <!-- CSS -->
    <link rel='stylesheet' href='<?= $front['bootstrap-css'] ?>'>
    <link rel='stylesheet' href='<?= $front['bootstrap-grid-css'] ?>'>
    <link rel='stylesheet' href='<?= $front['bootstrap-reboot-css'] ?>'>
    <link rel='stylesheet' href='<?= $front['custom-css'] ?>'>
    <link rel='stylesheet' href='<?= $front['font-awesome-css'] ?>'>

    <!-- JS -->
    <script type='text/javascript' src='<?= $front['jquery-js'] ?>'></script>
    <script type='text/javascript' src='<?= $front['jquery-tiny-js'] ?>'></script>
    <script type='text/javascript' src='<?= $front['season'] ?>'></script>
    <script type='text/javascript' src='<?= $front['bootstrap-bundle-js'] ?>'></script>
    <script type='text/javascript' src='<?= $front['tiny-js'] ?>'></script>
    <script type='text/javascript' src='<?= $front['tiny-init-js'] ?>'></script>
</head>
