<?php
/* *********************************************************************************************************************
 * Project name: Application
 * FIle name   : connection-state-buttons.php
 * Author      : Damien Nguyen
 * Date        : Friday, November 30 2018
 * ********************************************************************************************************************/

global $isAdministrator;
if (isset($_SESSION[ROLE])) { ?>
        
                <li class='nav-item dropdown'>
                    <a class='nav-link dropdown-toggle' href='#' id='navbarDropdown' role='button'
                            data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                        Menu
                    </a>
                    <div class='dropdown-menu bg-dark' aria-labelledby='navbarDropdown'><?php if ($isAdministrator) { ?>
                        
                        <a class='dropdown-item text-secondary' href='?action=displayPostCreationForm'>Create post</a>
                        <a class='dropdown-item text-secondary' href='?action=displayUsersList'>Manage users</a><?php } ?>
                        
                        <a class='dropdown-item text-secondary' href='?action=displayPasswordUpdateForm'>Update password</a>
                        <a class='dropdown-item text-secondary' href='?action=displayUserDetail&username=<?= $_SESSION[USERNAME] ?>'>Your profile</a>
                        <div class='dropdown-divider'></div>
                        <a class='dropdown-item text-secondary' href='?action=disconnect'>Disconnection</a>
                    </div>
                </li><?php } else { ?>

                <li class='nav-item'>
                    <a class='nav-link' href='?action=displayConnectionForm'>Connection</a>
                </li><?php }
