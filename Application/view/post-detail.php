<?php
/* *********************************************************************************************************************
 * Project name: Application
 * FIle name   : post-detail.php
 * Author      : Damien Nguyen
 * Date        : Friday, November 30 2018
 * ********************************************************************************************************************/

global $content, $isAdministrator;
?>
<!DOCTYPE html>
<html lang='en_US'>

<?php include($content['head']) ?>

<body>
    <nav class='bg-dark mb-4 navbar navbar-expand-lg navbar-dark'>
        <a class='font-lobster navbar-brand'>Post detail</a>
        <button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarSupportedContent'
                aria-controls='navbarSupportedContent' aria-expanded='false' aria-label='Toggle navigation'>
            <span class='navbar-toggler-icon'></span>
        </button>

        <div class='collapse navbar-collapse' id='navbarSupportedContent'>
            <ul class='mr-auto navbar-nav'>
                <li class='nav-item'>
                    <a class='nav-link' href='?'><i class='fa fa-users' aria-hidden='true'></i> Posts list</a>
                </li>
                <?php include(ROOT . $content['connection-state-buttons']) ?>

            </ul>
        </div>
        <?php if (isset($id) && $isAdministrator) { ?>
        
        <form class='text-center' method='post' action='?action=deletePosts'>
            <input type='hidden' name='id' value='<?= $id ?>'>
            <button class='btn btn-outline-danger' type='submit'>Delete</button>
        </form><?php } ?>
        
    </nav>

    <div class='container-fluid'>
        <table class='mb-4 slightly-transparent table table-bordered table-dark table-striped text-justify'><?php if (!isset($post) || empty($post)) { ?>
            
            <tr>
                <th>
                    <p class='font-weight-bold text-center text-danger'>Invalid post!</p>
                </th>
            </tr><?php } else { ?>
            
            <tr>
                <th>
                    <h1 class='text-center'><?= $post->getTitle() ?></h1>
                </th>
            </tr>
            <tr>
                <td class='text-secondary'>
                    Written by <a href='<?= $post->getAuthor() ?>'><?= $post->getAuthor() ?></a>
                    <span class='float-right text-secondary'><?= $post->getDate() ?></span>
                </td>
            </tr>
            <tr>
                <td>
                    <p class='text-justify'><?= $post->getContent() ?></p>
                </td>
            </tr><?php } ?>
            
            <tr>
                <th>
                    <h3 class='text-center'>Comments</h3>
                </th>
            </tr>

            <tr>
                <td>
                    <form class='text-center' method='post' action='?action=comment'>
                        <?php
                        if (isset($warning)) {
                            echo '<p class=\'font-weight-bold text-center text-danger\'>$warning</p>';
                        }
                        ?>
                        
                        <input name='id' type='hidden' value='<?= $id ?>'>
                        <input class='bg-dark border-secondary form-control mb-1 text-light' type='text'
                               name='author' <?= isset($author)
                                ? 'value=\'' . $author . '\' readonly'
                                : 'placeholder=\'Username\'' ?> required/>
                        <textarea class='bg-dark border-secondary form-control mb-1 text-light' name='comment'
                                  placeholder='Comment' rows='5'></textarea>
                        <button class='btn btn-outline-success' type='submit'>Submit</button>
                    </form>
                </td>
            </tr>
            <?php if (isset($comments) && !empty($comments)) {
                foreach ($comments as $comment) { ?>
            
            <tr>
                <td>
                    <p class='text-info'>
                        <?= $comment->getAuthor() ?> <span class='text-secondary'><?= $comment->getDate() ?></span><?php if ($isAdministrator) { ?>
                        
                        <span class='float-right'>
                            <form class='float-right' method='post' action='?action=deleteComment'>
                                <input type='hidden' name='deleteID' value='<?= $comment->getId() ?>'>
                                <input type='hidden' name='id' value='<?= $id ?>'>
                                <button class='btn btn-outline-danger' type='submit'>
                                    <i class='far fa-trash-alt text-danger'></i>
                                </button>
                            </form>
                        </span><?php } ?>
                        
                    </p>
                    <p class='text-justify'><?= $comment->getContent() ?></p>
                </td>
            </tr><?php }
            } ?>

        </table>
    </div>
</body>

</html>
