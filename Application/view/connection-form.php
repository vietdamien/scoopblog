<?php
/* *********************************************************************************************************************
 * Project name: Application
 * FIle name   : connection-form.php
 * Author      : Damien Nguyen
 * Date        : Tuesday, November 22 2018
 * ********************************************************************************************************************/

global $content;
?>
<!DOCTYPE html>
<html lang='en_US'>

<?php include(ROOT . $content['head']) ?>

<body>
    <div class='align-middle container'>
        <div class='align-items-center h-100 row'>
            <div class='col-md-3'></div>
            <div class='col-md-6'>
                <div class='bg-dark card p-2 slightly-transparent'>
                    <div class='card-header'>
                        <h2 class='text-center text-success'>Connection</h2>
                    </div>
                    <div class='card-body'>
                        <form class='mt-1 mb-2' method='POST' action='?action=connect'>
                            <?php if (isset($warning)) {
                                echo '<p class=\'font-weight-bold text-center text-danger\'>' . $warning . '</p>';
                            } elseif (isset($success)) {
                                echo '<p class=\'font-weight-bold text-center text-success\'>' . $success . '</p>';
                            } ?>

                            <div class='form-group'>
                                <label for='username'><i class='fa fa-user'></i> Username</label>
                                <input class='bg-dark form-control text-light' id='username' name='username' autofocus
                                       required>
                            </div>

                            <div class='form-group'>
                                <label for='password'><i class='fa fa-unlock-alt' aria-hidden='true'></i> Password</label>
                                <input class='bg-dark form-control text-light' id='password' type='password'
                                       name='password' required>
                            </div>

                            <a class='badge badge-secondary' href='?action=displayPasswordRecoveryForm'>
                                Forgot your password?
                            </a>

                            <button class='btn btn-block btn-md btn-success mt-5' type='submit' name='connect'>
                                Connection
                            </button>
                        </form>
                        <button class='btn btn-block btn-md btn-primary' type='submit' name='registration'
                                onclick='window.location.href="?action=displayRegistrationForm"'>
                            Registration
                        </button>
                        <button class='btn btn-block btn-md btn-outline-secondary' type='button'
                                onclick='window.location.href="?"'>
                            Cancel
                        </button>
                    </div>
                </div>
            </div>
            <div class='col-md-3'></div>
        </div>
    </div>
</body>

</html>
