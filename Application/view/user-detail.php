<?php
/* *********************************************************************************************************************
 * Project name: Application
 * FIle name   : user-detail.php
 * Author      : Damien Nguyen
 * Date        : Friday, November 30 2018
 * ********************************************************************************************************************/

global $content, $roles;
?>
<!DOCTYPE html>
<html lang='en_US'>

<?php include(ROOT . $content['head']);
if (isset($user)) { ?>

<body>
    <nav class='bg-dark mb-4 navbar navbar-dark navbar-expand-lg'>
        <a class='font-lobster navbar-brand'><?= isset ($currentSessionUsername)
                                                 && $currentSessionUsername == $user->getUserName()
                    ? 'My profile'
                    : 'User detail' ?></a>
        <button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarSupportedContent'
                aria-controls='navbarSupportedContent' aria-expanded='false' aria-label='Toggle navigation'>
            <span class='navbar-toggler-icon'></span>
        </button>

        <div class='collapse navbar-collapse' id='navbarSupportedContent'>
            <ul class='mr-auto navbar-nav'>
                <li class='nav-item'>
                    <a class='nav-link' href='?'><i class='fa fa-users' aria-hidden='true'></i> Posts list</a>
                </li>
                <?php include(ROOT . $content['connection-state-buttons']) ?>

            </ul>
        </div>
    </nav>

    <div class='container-fluid'>
        <table class='mb-4 slightly-transparent table table-bordered table-dark table-striped text-left'>
            <tr>
                <th colspan='8'>
                    <h1 class='text-center'><?= $user->getUsername() ?></h1>
                </th>
            </tr>
            <tr>
                <td>
                    <p class='font-weight-bold'>Email</p>
                </td>
                <td>
                    <a href='mailto:<?= $user->getEmail() ?>'><?= $user->getEmail() ?></a>
                </td>
            </tr>
            <tr>
                <td>
                    <p class='font-weight-bold'>Role</p>
                </td>
                <td>
                    <p><?= $user->getRole() ?></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p class='font-weight-bold'>Number of comments</p>
                </td>
                <td>
                    <p><?= $numberOfComments ?? DEFAULT_INT_VALUE ?></p>
                </td>
            </tr>

        </table>
    </div>
</body>

</html>

<?php }
