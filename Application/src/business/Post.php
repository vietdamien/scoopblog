<?php
/* *********************************************************************************************************************
 * Project name: Application
 * File name   : Post
 * Author      : Damien Nguyen
 * Date        : Thursday, November 29 2018
 * ********************************************************************************************************************/

/**
 * Class Post.
 * Only the administrators can write posts about whatever subject they would like.
 */
class Post
{
    /**
     * @var int the auto-incremented ID of this Post.
     */
    private $id;
    /**
     * @var string the title of this Post.
     */
    private $title;
    /**
     * @var string the content of this Post.
     */
    private $content;
    /**
     * @var string the date of publication of this Post.
     */
    private $date;
    /**
     * @var string the author of this Post.
     */
    private $author;

    /**
     * Post constructor.
     *
     * @param int    $id      the auto-incremented ID of this Post
     * @param string $title   the title of this Post
     * @param string $content the content of this Post
     * @param string $date    the date of publication of this Post
     * @param string $author  the author of this Post
     */
    public function __construct(int $id, string $title, string $content, string $date, string $author)
    {
        $this->id      = $id;
        $this->title   = $title;
        $this->content = $content;
        $this->date    = $date;
        $this->author  = $author;
    }

    /**
     * Gets the ID of this Post.
     *
     * @return int the ID of this Post
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Gets the title of this Post.
     *
     * @return string the title of this Post
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * Gets the content of this Post.
     *
     * @return string the content of this Post
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * Gets the date of publication of this Post.
     *
     * @return string the date of publication of this Post
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * Gets the author of this Post.
     *
     * @return string the author of this Post
     */
    public function getAuthor(): string
    {
        return $this->author;
    }
}
