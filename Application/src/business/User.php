<?php
/* *********************************************************************************************************************
 * Project name: Application
 * File name   : User
 * Author      : Damien Nguyen
 * Date        : Friday, November 23 2018
 * ********************************************************************************************************************/

/**
 * Class User.
 * This represents whoever is registered in the database.
 */
class User
{
    /**
     * @var string the username of this User
     */
    private $username;
    /**
     * @var string the email address of this User
     */
    private $email;
    /**
     * @var string the role of this User
     */
    private $role;

    /**
     * User constructor.
     *
     * @param string $username the username of the User
     * @param string $email    the email address of the User
     * @param string $role     the role of the User
     */
    public function __construct(string $username, string $email, string $role)
    {
        $this->username = $username;
        $this->email    = $email;
        $this->role     = $role;
    }

    /**
     * Gets the username of this User,
     *
     * @return string the username of this User
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * Gets the email address of this User.
     *
     * @return string the email address of this User
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * Gets the role of this User.
     *
     * @return string the role of this User
     */
    public function getRole(): string
    {
        return $this->role;
    }
}
