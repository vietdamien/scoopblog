<?php
/* *********************************************************************************************************************
 * Project name: Application
 * File name   : Comment
 * Author      : Raphaël Rouseyrol
 * Date        : Wednesday, December 5 2018
 * ********************************************************************************************************************/

/**
 * Class Comment.
 * This can be written by virtually anyone in reply to a Post.
 */
class Comment
{
    /**
     * @var int the auto-incremented ID of this Comment.
     */
    private $id;

    /**
     * @var string the content of this Comment.
     */
    private $content;

    /**
     * @var string the date of publication of this Comment.
     */
    private $date;

    /**
     * @var string the author of this Comment.
     */
    private $author;

    /**
     * Comment constructor.
     *
     * @param int    $id      the auto-incremented ID of this Comment
     * @param string $content the content of this Comment
     * @param string $date    the date of publication of this Comment
     * @param string $author  the author of this Comment
     */
    public function __construct(int $id, string $content, string $date, string $author)
    {
        $this->id      = $id;
        $this->content = $content;
        $this->date    = $date;
        $this->author  = $author;
    }

    /**
     * Gets the ID of this Comment.
     *
     * @return int the ID of this Comment
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Gets the content of this Comment.
     *
     * @return string the content of this Comment
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * Gets the date of publication of this Comment.
     *
     * @return string the date of publication of this Comment
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * Gets the author of this Comment.
     *
     * @return string the author of this Comment
     */
    public function getAuthor(): string
    {
        return $this->author;
    }
}
