<?php
/* *********************************************************************************************************************
 * Project name: Application
 * File name   : Validation
 * Author      : Damien Nguyen
 * Date        : Tuesday, November 20 2018
 * ********************************************************************************************************************/

/**
 * Class Validation.
 * This class validates user inputs in different ways.
 */
class Validation
{
    /**
     * Checks whether the given method name from the given origin class is static.
     *
     * @param object $originClass the given origin class
     * @param string $methodName  the given method name
     *
     * @return bool true if the method is static, false otherwise
     */
    public static function isStaticMethod($originClass, string $methodName): bool
    {
        try {
            return (new ReflectionMethod(get_class($originClass), $methodName))->isStatic();
        } catch (ReflectionException $e) {
            return false;
        }
    }

    /**
     * Checks whether the given email address is valid.
     *
     * @param mixed $email the given email address
     *
     * @return bool true if it is valid, false otherwise
     */
    public static function validateEmail($email = null): bool
    {
        return isset($email)
               && !empty($email)
               && filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    /**
     * Checks whether the given int value is valid.
     *
     * @param mixed $int the given int value
     *
     * @return bool true if it is valid, false otherwise
     */
    public static function validateInt($int = null): bool
    {
        return isset($int)
               && !empty($int)
               && $int != null
               && filter_var($int, FILTER_VALIDATE_INT);
    }

    /**
     * Checks whether the given page size is between the defined bounds.
     *
     * @param null $pageSize the given page size
     *
     * @return bool true if it is between the defined bounds, false otherwise
     */
    public static function validatePageSize($pageSize = null): bool
    {
        return ($pageSize = self::validateInt($pageSize))
               && $pageSize >= MINIMUM_PAGE_SIZE
               && $pageSize <= MAXIMUM_PAGE_SIZE;
    }

    /**
     * Checks whether the given password is a string, contains at least one uppercase letter, at least one lowercase
     * letter, at least one number and has a length of at least 8 characters.
     *
     * @param mixed $password the given password
     *
     * @return bool true if it is a valid password, false otherwise
     */
    public static function validatePassword($password = null): bool
    {
        return self::validateString($password)
               && preg_match(UPPERCASE_PATTERN, $password)
               && preg_match(LOWERCASE_PATTERN, $password)
               && preg_match(DIGITS_PATTERN, $password)
               && strlen($password) >= MINIMUM_PASSWORD_LENGTH;
    }

    /**
     * Checks whether the given role matches the current role from the $_SESSION[ROLE] variable.
     *
     * @param mixed $role the given role
     *
     * @return bool true if the role matches the current role, false otherwise
     */
    public static function validateSessionRole($role = null): bool
    {
        return isset($_SESSION[ROLE])
               && hash_equals(Cleaner::cleanString($_SESSION[ROLE]), crypt($role, CRYPT_STRING));
    }

    /**
     * Checks whether the given string is valid.
     *
     * @param mixed $string the given string
     *
     * @return bool true if it is valid, false otherwise
     */
    public static function validateString($string = null): bool
    {
        return isset($string)
               && !empty($string)
               && $string != null
               && $string == filter_var($string, FILTER_SANITIZE_STRING);
    }
}
