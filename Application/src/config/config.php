<?php
/* *********************************************************************************************************************
 * Project name: Application
 * FIle name   : config.php
 * Author      : Damien Nguyen
 * Date        : Tuesday, November 20 2018
 * ********************************************************************************************************************/

/* ========== CONSTANTS ========== */
define('ALLOWED_CHARACTERS', '!@#$%&*-_/?.0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
define('CRYPT_STRING', '$5$rounds=93438$/Bnxxk3qCjzowsAW$6Cai.8K7QWPiDxsHcTPCTyie1UuWEOG/OHFF9SyO8k3');
define('DATABASE', 'database');
define('DEFAULT_COOKIE_TIME', time() + 30 * 24 * 3600);
define('DEFAULT_INT_VALUE', 0);
define('DEFAULT_PAGE_SIZE', 10);
define('DEFAULT_PASSWORD_LENGTH', 12);
define('DUPLICATION', 23000);
define('FIRST_ELEMENT', 0);
define('HEADER_INDEX_LOCATION', 'Location: ?');
define('MINIMUM_COUNTER_VALUE', 1);
define('MAXIMUM_PAGE_SIZE', 50);
define('MINIMUM_PAGE_SIZE', 1);
define('MINIMUM_PAGE_VALUE', 1);
define('MINIMUM_PASSWORD_LENGTH', 8);

/* ========== DATABASE CREDENTIALS ========== */
$dsn        = 'mysql:host=localhost; dbname=ScoopBlog; charset=utf8mb4';
$dbUsername = 'scoopblog';
$dbPassword = 'Sc00PbL0G_AcCeSs01*!';

/* ========== EMAIL UTILITY ========== */
$headers = 'From: "ScoopBlog" <no-reply@damiennguyen.com>';

/* ========== MAPPINGS (VIEW) ========== */
define('ADMINISTRATOR', 'Administrator');
define('COMMENT', 'Comment');
define('DATE', 'date');
define('MEMBER', 'Member');
define('INVALID_CREDENTIALS', 'Invalid credentials.');
define('INVALID_EMAIL_FORMAT', 'This is not a valid email address.');
define('KEY_WORD', 'key word');
define('PASSWORD_INSTRUCTIONS', 'Your password should have an uppercase letter, a lowercase letter, a number and a '
                                . 'length of 8 characters minimum.');
define('PASSWORDS_NOT_IDENTICAL', 'Your passwords do not match.');
define('POST', 'Post');
define('UNKNOWN_DATA_SOURCE', 'Unknown data source!');
define('USER', 'User');
define('VISITOR', 'Visitor');

$roles = [
        crypt(ADMINISTRATOR, CRYPT_STRING) => ADMINISTRATOR,
        crypt(MEMBER, CRYPT_STRING)        => MEMBER,
];

/* ========== REGEX PATTERNS ========== */
define('UPPERCASE_PATTERN', '@[A-Z]@');
define('LOWERCASE_PATTERN', '@[a-z]@');
define('DIGITS_PATTERN', '@[0-9]@');

/* ========== ROOT DIRECTORY ========== */
const ROOT   = __DIR__ . '/../../';
const SOURCE = ROOT . 'src/';

/* ========== STRING MAPPINGS (VARIABLES) ========== */
define('CONTROLLER', 'Controller');
define('FACTORY', 'Factory');
define('MESSAGES_COUNTER', 'messagesCounter');
define('PAGE_SIZE', 'pageSize');
define('ROLE', 'role');
define('USERNAME', 'username');

/* ========== VIEW COMPONENTS ========== */
$content['connection-state-buttons'] = 'view/content/connection-state-buttons.php';
$content['head']                     = 'view/content/head.php';
$front['bootstrap-css']              = 'view/css/bootstrap.css';
$front['bootstrap-grid-css']         = 'view/css/bootstrap-grid.css';
$front['bootstrap-reboot-css']       = 'view/css/bootstrap-reboot.css';
$front['bootstrap-bundle-js']        = 'view/js/bootstrap.bundle.js';
$front['custom-css']                 = 'view/css/custom.css';
$front['favicon']                    = 'view/img/favicon.png';
$front['font-awesome-css']           = 'view/css/all.css';
$front['jquery-js']                  = 'view/js/jquery.js';
$front['jquery-tiny-js']             = 'view/js/jquery.tinymce.min.js';
$front['season']                     = 'view/js/season.js';
$front['tiny-init-js']               = 'view/js/tinymce-init.js';
$front['tiny-js']                    = 'view/js/tinymce.min.js';
$view['connection-form']             = 'view/connection-form.php';
$view['error']                       = 'view/error.php';
$view['password-recovery-form']      = 'view/password-recovery-form.php';
$view['password-update-form']        = 'view/password-update-form.php';
$view['post-creation-form']          = 'view/post-creation-form.php';
$view['post-detail']                 = 'view/post-detail.php';
$view['posts-list']                  = 'view/posts-list.php';
$view['registration-form']           = 'view/registration-form.php';
$view['user-detail']                 = 'view/user-detail.php';
$view['users-list']                  = 'view/users-list.php';
