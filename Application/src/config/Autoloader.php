<?php
/* *********************************************************************************************************************
 * Project name: Application
 * File name   : Autoloader
 * Author      : Damien Nguyen
 * Date        : Tuesday, November 20 2018
 * ********************************************************************************************************************/

/**
 * Class Autoloader.
 * This class loads every class in the specified folders.
 */
class Autoloader
{
    /**
     * The PHP file extension.
     */
    protected const PHP_EXTENSION = '.php';

    /**
     * @var Autoloader|null the unique instance of Autoloader.
     */
    private static $_instance = null;

    /**
     * @var RecursiveDirectoryIterator|null the recursive directory iterator to look for the class to load.
     */
    private static $fileIterator = null;

    /**
     * Creates a single instance of Autoloader.
     */
    public static function load()
    {
        if (null !== self::$_instance) {
            throw new RuntimeException(sprintf('%s is already started.', __CLASS__));
        }

        self::$_instance = new self();

        if (!spl_autoload_register([self::$_instance, '_autoload'], false)) {
            throw new RuntimeException(sprintf('%s: autoloader was not started.', __CLASS__));
        }
    }

    /**
     * Puts the instance of Autoloader back to null.
     */
    public static function shutDown()
    {
        if (null !== self::$_instance) {
            if (!spl_autoload_unregister(array(self::$_instance, '_autoload'))) {
                throw new RuntimeException('autoloader was not stopped.');
            }

            self::$_instance = null;
        }
    }

    /**
     * Loads all the classes.
     *
     * @param $class string the class name
     */
    private static function _autoload(string $class)
    {
        $directory = new RecursiveDirectoryIterator(SOURCE, RecursiveDirectoryIterator::SKIP_DOTS);

        if (is_null(self::$fileIterator)) {
            self::$fileIterator = new RecursiveIteratorIterator(
                    $directory,
                    RecursiveIteratorIterator::SELF_FIRST,
                    RecursiveIteratorIterator::CATCH_GET_CHILD
            );
        }

        $fileName = $class . self::PHP_EXTENSION;
        foreach (self::$fileIterator as $file) {
            if (strtolower($file->getFileName()) === strtolower($fileName) && $file->isReadable()) {
                include_once $file->getPathName();
            }
        }
    }
}
