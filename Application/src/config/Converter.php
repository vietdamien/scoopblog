<?php
/* *********************************************************************************************************************
 * Project name: Application
 * File name   : Converter
 * Author      : Damien Nguyen
 * Date        : Sunday, December 09 2018 
 * ********************************************************************************************************************/

/**
 * Class Converter.
 * This class converts data to more convenient formats.
 */
class Converter
{
    /**
     * Converts the given string to a more human-friendly date format.
     *
     * @param string $date the given string
     *
     * @return false|string a more human-friendly date format if the conversion was successful, false otherwise
     */
    public static function convertDate(string $date)
    {
        return date('M jS Y (h:iA)', strtotime($date));
    }
}
