<?php
/* *********************************************************************************************************************
 * Project name: Application
 * File name   : Cleaner
 * Author      : Damien Nguyen
 * Date        : Tuesday, November 20 2018
 * ********************************************************************************************************************/

/**
 * Class Cleaner.
 * This class cleans user inputs in different ways.
 */
class Cleaner
{
    /**
     * Cleans the given integer counter to match the defined counter minimum.
     *
     * @param null $int the given int value
     *
     * @return int|null a clean counter, null if it failed
     */
    public static function cleanCounter($int = null): int
    {
        return ($int = self::cleanInt($int)) <= DEFAULT_INT_VALUE ? MINIMUM_COUNTER_VALUE : $int;
    }

    /**
     * Cleans the given integer value.
     *
     * @param mixed $int the given integer value
     *
     * @return int|mixed a clean integer value, false if it failed
     */
    public static function cleanInt($int = null): int
    {
        return (!isset($int) || $int == null || !Validation::validateInt($int))
                ? DEFAULT_INT_VALUE
                : filter_var($int, FILTER_SANITIZE_NUMBER_INT);
    }

    /**
     * Cleans the given integer value to match the page number bounds.
     *
     * @param mixed $numberOfPages the number of pages
     * @param mixed $int           the given integer value
     *
     * @return int|mixed a clean integer value, false if it failed
     */
    public static function cleanPage($numberOfPages, $int = null): int
    {
        return ($page = self::cleanInt($int)) < MINIMUM_PAGE_VALUE
                ? MINIMUM_PAGE_VALUE
                : ($page > $numberOfPages ? $numberOfPages : $page);
    }

    /**
     * Cleans the given page size to match the defined page size bounds.
     *
     * @param null $pageSize the given page size
     *
     * @return int a clean page size
     */
    public static function cleanPageSize($pageSize = null): int
    {
        return Validation::validatePageSize($pageSize) ? $pageSize : DEFAULT_PAGE_SIZE;
    }

    /**
     * Cleans the given string by removing all the unwanted characters and gets either KEY_WORD or DATE.
     *
     * @param mixed $keyType the given string
     *
     * @return mixed a clean string that represents the search type, false if it failed
     */
    public static function cleanSearchType($keyType = null): string
    {
        return self::cleanString($keyType) == KEY_WORD ? KEY_WORD : DATE;
    }

    /**
     * Cleans the given string by removing all the unwanted characters.
     *
     * @param mixed $string the given string
     *
     * @return mixed a clean string, false if it failed
     */
    public static function cleanString($string = null): string
    {
        return filter_var($string, FILTER_SANITIZE_STRING);
    }
}
