<?php
/* *********************************************************************************************************************
 * Project name: Application
 * File name   : Gateway
 * Author      : Damien Nguyen
 * Date        : Wednesday, November 28 2018
 * ********************************************************************************************************************/

/**
 * Class Gateway.
 * This class is the parent class of all gateways.
 */
abstract class Gateway
{
    /**
     * @var Connection the instance of Connection necessary for any SQL query.
     */
    protected $connection;

    /**
     * Gateway constructor.
     *
     * @throws Exception if there was a PDO related error
     */
    public function __construct()
    {
        global $dsn, $dbUsername, $dbPassword;
        $this->connection = new Connection($dsn, $dbUsername, $dbPassword);
    }
}
