<?php
/* *********************************************************************************************************************
 * Project name: Application
 * File name   : Connection
 * Author      : Damien Nguyen
 * Date        : Tuesday, November 20 2018
 * ********************************************************************************************************************/

/**
 * Class Connection.
 * This class allows the connection to the MySQL database as well as SQL queries.
 */
class Connection extends PDO
{
    /**
     * The index of the parameter value to bind.
     */
    private const PARAMETER_VALUE_INDEX = 0;
    /**
     * The index of the parameter value type to bind.
     */
    private const PARAMETER_TYPE_INDEX = 1;
    /**
     * @var PDOStatement the SQL statement
     */
    private $statement;

    /**
     * Connection constructor.
     *
     * @param $dsn      string the data source name
     * @param $username string the username for the database
     * @param $password string the associated password
     *
     * @throws Exception if the connection to the database failed
     */
    public function __construct(string $dsn, string $username, string $password)
    {
        try {
            parent::__construct($dsn, $username, $password);
            $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            throw new Exception('Connection to base failed! ' . $e->getMessage());
        }
    }

    /**
     * Checks whether the deletion was successful.
     *
     * @return bool true if the deletion was successful, false otherwise
     */
    public function updateSucceeded(): bool
    {
        return $this->statement->rowCount() != DEFAULT_INT_VALUE;
    }

    /**
     * Executes the given SQL query with the given array of parameters.
     *
     * @param       $query      string the given SQL query
     * @param array $parameters the given array of parameters
     *
     * @return bool true if the query was executed, false otherwise
     * @throws Exception if the query broke a constraint or is impossible
     */
    public function executeQuery(string $query, array $parameters = []): bool
    {
        $this->statement = parent::prepare($query);
        foreach ($parameters as $name => $value) {
            $this->statement->bindValue($name, $value[self::PARAMETER_VALUE_INDEX], $value[self::PARAMETER_TYPE_INDEX]);
        }
        return $this->statement->execute();
    }

    /**
     * Gets the results of a SQL query.
     *
     * @return array the results of a SQL query
     * @throws Exception if there was an error fetching the results
     */
    public function getResults(): array
    {
        return $this->statement->fetchall();
    }
}
