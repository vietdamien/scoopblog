<?php
/* *********************************************************************************************************************
 * Project name: Application
 * File name   : UserGateway
 * Author      : Damien Nguyen
 * Date        : Friday, November 23 2018
 * ********************************************************************************************************************/

/**
 * Class UserGateway.
 * This class is the gateway between the USER table in the database and the VisitorModel.
 */
class UserGateway extends Gateway
{
    /**
     * Checks if the given username and the given password match the credentials stored in the database to allow the
     * User to be connected.
     *
     * @param string $username the username of the User
     * @param string $password the password of the User
     *
     * @return bool true if the verification was successful, false otherwise
     * @throws Exception if the query failed
     */
    public function connect(string $username, string $password): bool
    {
        $this->connection->executeQuery(
                'SELECT `password` FROM `USER` WHERE `username` = :username',
                [':username' => [$username, PDO::PARAM_STR]]
        );
        return password_verify($password, $this->connection->getResults()[FIRST_ELEMENT]['password']);
    }

    /**
     * Deletes the User that has the given username.
     *
     * @param string $username the given username
     *
     * @return bool true if the query was executed, false otherwise
     * @throws Exception if the deletion of the User failed
     */
    public function delete(string $username): bool
    {
        $this->connection->executeQuery(
                'DELETE FROM `USER` WHERE `username` = :username; ALTER TABLE `USER` AUTO_INCREMENT = 0;
                        ALTER TABLE `POST` AUTO_INCREMENT = 0;
                        ALTER TABLE `COMMENT` AUTO_INCREMENT = 0',
                [':username' => [$username, PDO::PARAM_STR]]
        );
        return $this->connection->updateSucceeded();
    }

    /**
     * Adds a new User in the database after registration.
     *
     * @param string $username the username of the User
     * @param string $password the hashed password of the User
     * @param string $email    the email address of the User
     * @param string $role     the role of the User
     *
     * @return string the last insert ID
     * @throws Exception if the registration of the new User failed
     */
    public function insert(string $username, string $password, string $email, string $role): string
    {
        $this->connection->executeQuery(
                'INSERT INTO `USER` VALUES (:username, :password, :email, :role);',
                [':username' => [$username, PDO::PARAM_STR], ':password' => [$password, PDO::PARAM_STR],
                 ':email'    => [$email, PDO::PARAM_STR], ':role' => [$role, PDO::PARAM_STR]]
        );
        return $this->connection->lastInsertId();
    }

    /**
     * Retrieves all the users from the database that correspond the given key string or not.
     *
     * @param string|null $keyString the given key string
     *
     * @return array all the users and their attributes
     * @throws Exception if the retrieval of all the users failed
     */
    public function retrieve(string $keyString = null): array
    {
        $query = 'SELECT `username`, `email`, `role` FROM `USER`';
        if ($keyString) {
            $query .= ' WHERE `username` LIKE \'%' . $keyString . '%\'
                        OR    `email`    LIKE \'%' . $keyString . '%\'';
        }
        $query .= ' ORDER BY `username` ASC';
        $this->connection->executeQuery($query);

        return Factory::getFactory($this)->createArray($this->connection->getResults());
    }

    /**
     * Retrieves the role of the User that has the given username.
     *
     * @param string $username the given username
     *
     * @return string the role of the User
     * @throws Exception if the retrieval of the role failed
     */
    public function retrieveRole(string $username): string
    {
        $this->connection->executeQuery(
                'SELECT `role` FROM `USER` WHERE `username` = :username',
                [':username' => [$username, PDO::PARAM_STR]]
        );
        return $this->connection->getResults()[FIRST_ELEMENT]['role'];
    }

    /**
     * Retrieves the User that has the given username from the database.
     *
     * @param string $username the given username
     *
     * @return User the User that has the given username
     * @throws Exception if the retrieval of the users failed or the User was not found
     */
    public function retrieveUser(string $username): User
    {
        $this->connection->executeQuery(
                'SELECT `username`, `email`, `role` FROM `USER` WHERE `username` = :username;',
                [':username' => [$username, PDO::PARAM_STR]]
        );
        $result = $this->connection->getResults()[FIRST_ELEMENT];
        if (!$result) {
            throw new Exception('This user ($username) was not found. Please contact an administrator.');
        }

        return Factory::getFactory($this)->create($result);
    }

    /**
     * Updates and sets the given hashed password to the User who has the given username or email in the database.
     *
     * @param string $identifier the given username or email
     * @param string $password   the given hashed password
     *
     * @return bool true if the update was successful, false otherwise
     * @throws Exception if the update failed
     */
    public function updatePassword(string $identifier, string $password): bool
    {
        $this->connection->executeQuery(
                'UPDATE `USER` SET `password` = :pass WHERE `username` = :identifier OR `email` = :identifier',
                [':identifier' => [$identifier, PDO::PARAM_STR], ':pass' => [$password, PDO::PARAM_STR]]
        );
        return $this->connection->updateSucceeded();
    }

    /**
     * Updates and sets the given encrypted role to the User who has the given username in the database.
     *
     * @param string $username the given username
     * @param string $role     the given encrypted role
     *
     * @return bool true if the update was successful, false otherwise
     * @throws Exception if the update failed
     */
    public function updateRole(string $username, string $role): bool
    {
        $this->connection->executeQuery(
                'UPDATE `USER` SET `role` = :role WHERE `username` = :username',
                [':role' => [$role, PDO::PARAM_STR], ':username' => [$username, PDO::PARAM_STR]]
        );
        return $this->connection->updateSucceeded();
    }
}
