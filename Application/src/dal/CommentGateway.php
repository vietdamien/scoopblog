<?php
/* *********************************************************************************************************************
 * Project name: Application
 * File name   : CommentGateway
 * Author      : Raphaël Rouseyrol
 * Date        : Wednesday, December 5 2018
 * ********************************************************************************************************************/

/**
 * Class CommentGateway.
 * This class is the gateway between the COMMENT table in the database and the CommentModel.
 */
class CommentGateway extends Gateway
{
    /**
     * Adds a new Comment in the database.
     *
     * @param string $comment the content of the new Comment
     * @param int    $toPost  the destination post of the new Comment
     * @param string $author  the author of the new Comment
     *
     * @return bool true if the addition was successful, false otherwise
     * @throws Exception if the addition of the new Comment failed
     */
    public function add(string $comment, int $toPost, string $author): bool
    {
        return $this->connection->executeQuery(
                'INSERT INTO `COMMENT` (`content`, `date`, `toPost`, `author`)
                        VALUES (:content, NOW(), :toPost, :author)',
                [':content' => [$comment, PDO::PARAM_STR],
                 ':toPost' => [$toPost, PDO::PARAM_INT],
                 ':author' => [$author, PDO::PARAM_STR]]
        );
    }

    /**
     * Counts the number of comments in the database according to the author username or not.
     *
     * @param string|null $author the given author username
     *
     * @return int the number of comments
     * @throws Exception if the query failed
     */
    public function count(string $author = null): int
    {
        $query = 'SELECT COUNT(1) FROM `COMMENT`';
        if ($author) {
            $query      .= ' WHERE `author` = :author';
            $parameters = [':author' => [$author, PDO::PARAM_STR]];
        }
        $this->connection->executeQuery($query, $parameters ?? []);
        return $this->connection->getResults()[FIRST_ELEMENT][FIRST_ELEMENT];
    }

    /**
     * Deletes the Comment that has the given ID.
     *
     * @param int $id the given ID
     *
     * @return bool true if the deletion was successful, false otherwise
     * @throws Exception if the query failed
     */
    public function delete(int $id): bool
    {
        $this->connection->executeQuery(
                'DELETE FROM `COMMENT` WHERE `id` = :id; ALTER TABLE `COMMENT` AUTO_INCREMENT = 0',
                [':id' => [$id, PDO::PARAM_INT]]
        );
        return $this->connection->updateSucceeded();
    }

    /**
     * Retrieves the comments from the database that correspond to the given Post ID.
     *
     * @param int $toPost the given Post ID
     *
     * @return array all the comments and their attributes
     * @throws Exception if the retrieval of all the comments failed
     */
    public function retrieve(int $toPost): array
    {
        $this->connection->executeQuery(
                'SELECT * FROM `COMMENT` WHERE `toPost` = :postId ORDER BY `date` DESC',
                [':postId' => [$toPost, PDO::PARAM_INT]]
        );
        return Factory::getFactory($this)->createArray($this->connection->getResults());
    }
}
