<?php
/* **************************************************************************************************
 * Project name: Application
 * File name   : PostGateway
 * Author      : Damien Nguyen
 * Date        : Friday, November 30 2018
 * **************************************************************************************************/

/**
 * Class PostGateway.
 * This class is the gateway between the POST table in the database and the PostModel.
 */
class PostGateway extends Gateway
{
    /**
     * Counts the number of posts in the database according to the given key string or not.
     *
     * @param string|null $keyString the given key string
     *
     * @return int the number of posts in the database
     * @throws Exception if the query failed
     */
    public function count(string $keyString = null): int
    {
        $query = 'SELECT COUNT(1) FROM `POST`';
        if ($keyString) {
            $query .= ' WHERE `title`   LIKE \'%' . $keyString . '%\'
                        OR    `content` LIKE \'%' . $keyString . '%\'
                        OR    `author`  LIKE \'%' . $keyString . '%\'';
        }
        $this->connection->executeQuery($query);
        return $this->connection->getResults()[FIRST_ELEMENT][FIRST_ELEMENT];
    }

    /**
     * Counts the number of pages according to the number of posts in the database according to the given key string or
     * not.
     *
     * @param int         $pageSize  the page size
     * @param string|null $keyString the given key string
     *
     * @return int the number of pages
     * @throws Exception if the retrieval of the number of pages failed
     */
    public function countPages(int $pageSize, string $keyString = null): int
    {
        return ceil($this->count($keyString) / $pageSize);
    }

    /**
     * Deletes the Post that has the given ID.
     *
     * @param int|null $id the given ID
     *
     * @return bool true if the query was executed, false otherwise
     * @throws Exception if the deletion of the Post failed
     */
    public function delete(int $id = null): bool
    {
        $query = 'DELETE FROM `POST`';
        if ($id) {
            $query      .= ' WHERE `id` = :id';
            $parameters = [':id' => [$id, PDO::PARAM_INT]];
        }
        $query .= '; ALTER TABLE `POST` AUTO_INCREMENT = 0; ALTER TABLE `COMMENT` AUTO_INCREMENT = 0';
        $this->connection->executeQuery($query, $parameters ?? []);
        return $this->connection->updateSucceeded();
    }

    /**
     * Adds a new Post with the given title, the given content and the given author in the database.
     *
     * @param string $title   the given title
     * @param string $content the given content
     * @param string $author  the given author
     *
     * @return string the ID of the new Post
     * @throws Exception if the insertion of the new Post failed
     */
    public function insert(string $title, string $content, string $author): string
    {
        $this->connection->executeQuery(
                'INSERT INTO `POST` (`title`, `content`, `date`, `author`)
                        VALUES (:title, :content, NOW(), :author)',
                [':title'   => [$title, PDO::PARAM_STR],
                 ':content' => [$content, PDO::PARAM_STR],
                 ':author'  => [$author, PDO::PARAM_STR]]
        );
        return $this->connection->lastInsertId();
    }

    /**
     * Retrieves the posts from the database that correspond to the given requested page and the given key string or
     * not.
     *
     * @param int         $requestedPage the given requested page
     * @param int         $pageSize      the page size
     * @param string|null $keyString     the given key string
     * @param string|null $keyType       the type of the given key string
     *
     * @return array all the posts and their attributes
     * @throws Exception if the retrieval of all the posts failed
     */
    public function retrieve(int $requestedPage, int $pageSize, string $keyString = null, string $keyType = null): array
    {
        $position = $requestedPage < MINIMUM_PAGE_VALUE ? MINIMUM_PAGE_VALUE : ($requestedPage - 1) * $pageSize;

        $query = 'SELECT * FROM `POST`';
        if ($keyString && $keyType) {
            if ($keyType == DATE) {
                $query               .= ' WHERE DATE(`date`) = :date';
                $parameters[':date'] = [$keyString, PDO::PARAM_STR];
            } else {
                $query .= ' WHERE `title`   LIKE \'%' . $keyString . '%\'
                        OR    `content` LIKE \'%' . $keyString . '%\'
                        OR    `author`  LIKE \'%' . $keyString . '%\'';
            }
        }
        $query            .= ' ORDER BY `date` DESC LIMIT :x, :y';
        $parameters[':x'] = [$position, PDO::PARAM_INT];
        $parameters[':y'] = [$pageSize, PDO::PARAM_INT];
        $this->connection->executeQuery($query, $parameters);

        return Factory::getFactory($this)->createArray($this->connection->getResults());
    }

    /**
     * Retrieves the Post that has the given ID from the database.
     *
     * @param int $id the given ID
     *
     * @return Post the Post that has the given ID
     * @throws Exception if the retrieval of the post failed or the Post was not found
     */
    public function retrievePost(int $id): Post
    {
        $this->connection->executeQuery(
                'SELECT * FROM `POST` WHERE `id` = :id',
                [':id' => [$id, PDO::PARAM_INT]]
        );
        $result = reset($this->connection->getResults());
        if (!$result) {
            throw new Exception('The post #' . $id . ' was not found. Please contact an administrator.');
        }

        return Factory::getFactory($this)->create($result);
    }
}
