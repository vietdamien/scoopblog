<?php
/* *********************************************************************************************************************
 * Project name: Application
 * File name   : PostModel
 * Author      : Damien Nguyen
 * Date        : Friday, November 30 2018
 * ********************************************************************************************************************/

/**
 * Class PostModel.
 * This class links the PostGateway and the controllers.
 */
class PostModel
{
    /**
     * Adds a new Post with the given title, the given content and the given author in the database.
     *
     * @param string $title   the given title
     * @param string $content the given content
     * @param string $author  the given author
     *
     * @return string the ID of the new Post
     * @throws Exception if there was a database related error
     */
    public static function add(string $title, string $content, string $author): string
    {
        return (new PostGateway())->insert($title, $content, $author);
    }

    /**
     * Deletes the Post that has the given ID.
     *
     * @param int|null $id the given ID
     *
     * @return bool true if the deletion was successful, false otherwise
     * @throws Exception if there was a database related error
     */
    public static function delete(int $id = null): bool
    {
        return (new PostGateway())->delete($id);
    }

    /**
     * Counts the number of pages according to the number of posts in the database according to the given key string or
     * not.
     *
     * @param int         $pageSize  the page size
     * @param string|null $keyString the given key string
     *
     * @return int the number of pages
     * @throws Exception if there was a database related error
     */
    public static function getNumberOfPages(int $pageSize, string $keyString = null): int
    {
        return (new PostGateway())->countPages($pageSize, $keyString);
    }

    /**
     * Retrieves the posts from the database that correspond to the given requested page and the given key string or
     * not.
     *
     * @param int         $requestedPage the given requested page
     * @param int         $pageSize      the page size
     * @param string|null $keyString     the given key string
     * @param string|null $keyType       the type of the given key string
     *
     * @return array all the posts and their attributes
     * @throws Exception if there was a database related error
     */
    public static function retrieve(int $requestedPage, int $pageSize, string $keyString = null,
                                    string $keyType = null): array
    {
        return (new PostGateway())->retrieve($requestedPage, $pageSize, $keyString, $keyType);
    }

    /**
     * Retrieves the Post that has the given ID from the database.
     *
     * @param int $id the given ID
     *
     * @return Post the Post that has the given ID
     * @throws Exception if there was a database related error
     */
    public static function retrievePost(int $id): Post
    {
        return (new PostGateway())->retrievePost($id);
    }
}
