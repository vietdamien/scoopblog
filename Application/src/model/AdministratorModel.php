<?php
/* *********************************************************************************************************************
 * Project name: Application
 * File name   : AdministratorModel
 * Author      : Damien Nguyen
 * Date        : Tuesday, December 18 2018 
 * ********************************************************************************************************************/

/**
 * Class AdministratorModel.
 * This class defines the actions that are related to administrators.
 */
class AdministratorModel
{
    /**
     * Deletes the User that has the given username.
     *
     * @param string $username the given username
     *
     * @return bool true if the deletion was successful, false otherwise
     * @throws Exception if there was a database related error
     */
    public static function delete(string $username): bool
    {
        return (new UserGateway())->delete($username);
    }

    /**
     * Checks whether the User is an administrator.
     *
     * @return bool true if the User is an administrator, false otherwise
     */
    public static function isAdministrator(): bool
    {
        return Validation::validateSessionRole(ADMINISTRATOR);
    }

    /**
     * Retrieves all the users from the database that correspond to the given username or not.
     *
     * @param string|null $username the given username
     *
     * @return array all the users and their attributes
     * @throws Exception if there was a database related error
     */
    public static function retrieve(string $username = null): array
    {
        return (new UserGateway())->retrieve($username);
    }

    /**
     * Updates and sets the given role to the User who has the given username.
     *
     * @param string $username the given username
     * @param string $role     the given role
     *
     * @return bool if the update was successful, false otherwise
     * @throws Exception if there was a database related error
     */
    public static function updateRole(string $username, string $role): bool
    {
        return (new UserGateway())->updateRole($username, crypt($role, CRYPT_STRING));
    }
}
