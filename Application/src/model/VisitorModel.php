<?php
/* *********************************************************************************************************************
 * Project name: Application
 * File name   : VisitorModel
 * Author      : Damien Nguyen
 * Date        : Friday, November 23 2018
 * ********************************************************************************************************************/

/**
 * Class VisitorModel.
 * This class defines the actions related to a basic user (visitor) and links the UserGateway and the controllers.
 */
class VisitorModel
{
    /**
     * Allows the User with the given username, the given password to connect and generates a warning if the connection
     * failed.
     *
     * @param string      $username the given username
     * @param string      $password the given password
     * @param string|null $warning  the warning in case of a failure
     *
     * @return bool true if the connection was successful, false otherwise
     * @throws Exception if there was a database error
     */
    public static function connect(string $username, string $password, string &$warning = null): bool
    {
        if ($isValid = ($dbAccess = new UserGateway())->connect($username, $password)) {
            $_SESSION[ROLE]     = Cleaner::cleanString($dbAccess->retrieveRole($username));
            $_SESSION[USERNAME] = Cleaner::cleanString($username);
        } else {
            $warning = INVALID_CREDENTIALS;
        }
        return $isValid;
    }

    /**
     * Registers a new User in the database. The role is set to MEMBER by default.
     *
     * @param string|null $username  the username of the new User
     * @param string|null $password  the password of the new User
     * @param string|null $passwordC the password confirmation of the new User
     * @param string|null $email     the email address of the new User
     * @param string|null $warning   the error output in case of a problem
     * @param string|null $success   the success output if the new User was successfully registered
     *
     * @return bool true if the new User was successfully registered, false otherwise
     * @throws Exception if there was a database related error
     */
    public static function register(string $username = null, string $password = null, string $passwordC = null,
                                    string $email = null, string &$warning = null, string &$success = null): bool
    {
        if (!($isValid = ($areIdentical = $password == $passwordC) && Validation::validatePassword($password))) {
            $warning = $areIdentical ? PASSWORD_INSTRUCTIONS : PASSWORDS_NOT_IDENTICAL;
        } elseif (!($isValid = Validation::validateEmail($email))) {
            $warning = INVALID_EMAIL_FORMAT;
        } else {
            try {
                (new UserGateway())->insert($username, password_hash($password, PASSWORD_BCRYPT), $email,
                                            crypt(MEMBER, CRYPT_STRING));
                $success = 'Welcome ' . $username . ', you are now registered.';
            } catch (Exception $e) {
                $warning = $e->getCode() == DUPLICATION
                        ? 'This username or email address is already registered!'
                        : 'Please try again.';
                return false;
            }
        }

        return $isValid;
    }

    /**
     * Resets the password of the User who has the given email address.
     *
     * @param string      $email   the given email address
     * @param string|null $warning the error output in case of a problem
     *
     * @return bool true if the password was reset, false otherwise
     * @throws Exception if there was a database error
     */
    public static function recoverPassword(string $email, string &$warning = null): bool
    {
        global $headers;
        if (!($isValid = Validation::validateEmail($email))) {
            $warning = INVALID_EMAIL_FORMAT;
        } else {
            $new = substr(str_shuffle(str_repeat(
                                              $allowedCharacters = ALLOWED_CHARACTERS,
                                              ceil(DEFAULT_PASSWORD_LENGTH / strlen($allowedCharacters))
                                      )), MINIMUM_COUNTER_VALUE, DEFAULT_PASSWORD_LENGTH
            );
            if ($isValid = (new UserGateway())->updatePassword($email, password_hash($new, PASSWORD_BCRYPT))) {
                mail($email, 'New password',
                     'Dear user,' . "\n\n" . 'You have requested a new password, here it is: ' . $new . "\n"
                     . 'It is strongly advised to change it right after logging in with this password.' . "\n\n"
                     . ' Kind regards,' . "\n" . 'ScoopBlog administration.', $headers);
            }
        }
        return $isValid;
    }

    /**
     * Retrieves the User that has the given username.
     *
     * @param string $username the given username
     *
     * @return User the User that has the given username
     * @throws Exception if there was a database related error
     */
    public static function retrieveUser(string $username): User
    {
        return (new UserGateway())->retrieveUser($username);
    }
}
