<?php
/* *********************************************************************************************************************
 * Project name: Application
 * File name   : CommentModel
 * Author      : Raphaël Rouseyrol
 * Date        : Wednesday, December 5 2018
 * ********************************************************************************************************************/

/**
 * Class CommentModel.
 * This class links the CommentGateway and the controllers.
 */
class CommentModel
{
    /**
     * Adds a new Comment in the database.
     *
     * @param string $comment the content of the new Comment
     * @param int    $toPost  the destination of the new Comment
     * @param string $author  the author of the new Comment
     *
     * @return bool true if the addition was successful, false otherwise
     * @throws Exception if there was a database related Error
     */
    public static function add(string $comment, int $toPost, string $author): bool
    {
        if (!isset($_SESSION[USERNAME])) {
            $_SESSION[USERNAME] = $author;
        }
        return (new CommentGateway())->add($comment, $toPost, $author);
    }

    /**
     * Counts the number of comments in the database according to the author username or not.
     *
     * @param string|null $author the given author username
     *
     * @return int the number of comments
     * @throws Exception if there was a database related error
     */
    public static function count(string $author = null): int
    {
        return (new CommentGateway())->count($author);
    }

    /**
     * Deletes the Comment that has the given ID.
     *
     * @param int $id the given ID
     *
     * @return bool true if the deletion was successful, false otherwise
     * @throws Exception if there was a database related error
     */
    public static function delete(int $id): bool
    {
        return (new CommentGateway())->delete($id);
    }

    /**
     * Retrieves the comments from the database that correspond to the given Post ID.
     *
     * @param int $destinationPost the given Post ID
     *
     * @return array all the comments and their attributes
     * @throws Exception if there was a database related error
     */
    public static function retrieve(int $destinationPost): array
    {
        return (new CommentGateway())->retrieve($destinationPost);
    }
}
