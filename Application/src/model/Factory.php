<?php
/* *********************************************************************************************************************
 * Project name: Application
 * File name   : Factory
 * Author      : Damien Nguyen
 * Date        : Thursday, December 13 2018 
 * ********************************************************************************************************************/

/**
 * Class Factory.
 * This class creates objects by giving this responsibility to the right Factory.
 */
abstract class Factory
{
    /**
     * Gets the right Factory with the given source Gateway.
     *
     * @param Gateway $source the source Gateway
     *
     * @return CommentFactory|PostFactory|UserFactory the right Factory
     */
    static function getFactory(Gateway $source): Factory
    {
        $className = str_replace(Gateway::class, '', get_class($source)) . FACTORY;
        return new $className();
    }

    /**
     * Creates an object with the given array of attributes.
     *
     * @param array  $array  the given array of attributes
     * @param string $source the data source
     *
     * @return mixed an object
     */
    abstract function create(array $array, string $source = DATABASE);

    /**
     * Creates an array of objects with the given array of attributes.
     *
     * @param array  $array  the given array of attributes
     * @param string $source the data source
     *
     * @return array an array of objects
     */
    abstract function createArray(array $array, string $source = DATABASE): array;
}
