<?php
/* *********************************************************************************************************************
 * Project name: Application
 * File name   : UserFactory
 * Author      : Damien Nguyen
 * Date        : Friday, December 07 2018
 * ********************************************************************************************************************/

/**
 * Class UserFactory.
 * This class creates instances of users.
 */
class UserFactory extends Factory
{
    /**
     * Creates a User with the given array of attributes.
     *
     * @param array  $array  the given array of attributes
     * @param string $source the data source
     *
     * @return User a User
     */
    function create(array $array, string $source = DATABASE): User
    {
        global $roles;
        switch ($source) {
            case DATABASE:
                return new User($array['username'], $array['email'], $roles[$array['role']]);
            default:
                throw new InvalidArgumentException(UNKNOWN_DATA_SOURCE);
        }
    }

    /**
     * Creates an array of objects with the given array of attributes.
     *
     * @param array  $array  the given array of attributes
     * @param string $source the data source
     *
     * @return array an array of objects
     */
    function createArray(array $array, string $source = DATABASE): array
    {
        switch ($source) {
            case DATABASE:
                foreach ($array as $user) {
                    $users[] = $this->create($user);
                }
                return $users ?? [];
            default:
                throw new InvalidArgumentException(UNKNOWN_DATA_SOURCE);
        }
    }
}
