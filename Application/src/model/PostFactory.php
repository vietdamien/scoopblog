<?php
/* *********************************************************************************************************************
 * Project name: Application
 * File name   : PostFactory
 * Author      : Damien Nguyen
 * Date        : Friday, December 07 2018
 * ********************************************************************************************************************/

/**
 * Class PostFactory.
 * This class creates instances of posts.
 */
class PostFactory extends Factory
{
    /**
     * Creates a Post with the given array of attributes.
     *
     * @param array  $array  the given array of attributes
     * @param string $source the data source
     *
     * @return Post a Post
     */
    function create(array $array, string $source = DATABASE): Post
    {
        switch ($source) {
            case DATABASE:
                return new Post($array['id'], $array['title'], $array['content'],
                                Converter::convertDate($array['date']),
                                $array['author']);
            default:
                throw new InvalidArgumentException(UNKNOWN_DATA_SOURCE);
        }
    }

    /**
     * Creates an array of posts with the given array of attributes.
     *
     * @param array  $array  the given array of attributes
     * @param string $source the data source
     *
     * @return array an array of posts
     */
    function createArray(array $array, string $source = DATABASE): array
    {
        switch ($source) {
            case DATABASE:
                foreach ($array as $post) {
                    $posts[] = $this->create($post);
                }
                return $posts ?? [];
            default:
                throw new InvalidArgumentException(UNKNOWN_DATA_SOURCE);
        }
    }
}
