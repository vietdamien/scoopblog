<?php
/* *********************************************************************************************************************
 * Project name: Application
 * File name   : CommentFactory
 * Author      : Damien Nguyen
 * Date        : Thursday, December 13 2018 
 * ********************************************************************************************************************/

class CommentFactory extends Factory
{
    /**
     * Creates a Comment with the given array of attributes.
     *
     * @param array  $array  the given array of attributes
     * @param string $source the data source
     *
     * @return Comment a Comment
     */
    function create(array $array, string $source = DATABASE): Comment
    {
        switch ($source) {
            case DATABASE:
                return new Comment($array['id'], $array['content'], Converter::convertDate($array['date']),
                                   $array['author']);
            default:
                throw new InvalidArgumentException(UNKNOWN_DATA_SOURCE);
        }
    }

    /**
     * Creates an array of objects with the given array of attributes.
     *
     * @param array  $array  the given array of attributes
     * @param string $source the data source
     *
     * @return array an array of objects
     */
    function createArray(array $array, string $source = DATABASE): array
    {
        switch ($source) {
            case DATABASE:
                foreach ($array as $comment) {
                    $comments[] = $this->create($comment);
                }
                return $comments ?? [];
            default:
                throw new InvalidArgumentException(UNKNOWN_DATA_SOURCE);
        }
    }
}
