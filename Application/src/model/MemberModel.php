<?php
/* *********************************************************************************************************************
 * Project name: Application
 * File name   : MemberModel
 * Author      : Damien Nguyen
 * Date        : Tuesday, December 18 2018 
 * ********************************************************************************************************************/

/**
 * Class MemberModel.
 * This class defines the actions related to a member and links the UserGateway and the controllers.
 */
class MemberModel
{
    /**
     * Destroys the ongoing session to disconnect the User.
     */
    public static function disconnect()
    {
        session_unset();
        session_destroy();
        $_SESSION = [];
    }

    /**
     * Checks whether the User is a member or higher.
     *
     * @return bool true if the User is a member, false otherwise
     */
    public static function isMember(): bool
    {
        return isset($_SESSION[ROLE]);
    }

    /**
     * Updates and sets the given password to the User who has the given username. The old password is required and a
     * confirmation is also prompted to avoid any mistake with the new password.
     *
     * @param string      $username the given username
     * @param string|null $old      the old password
     * @param string|null $new      the new password
     * @param string|null $newC     the new password confirmation
     * @param string|null $warning  the warning in case of a failure
     *
     * @return bool if the update was successful, false otherwise
     * @throws Exception if there was a database related error
     */
    public static function updatePassword(string $username, string $old = null, string $new = null, string $newC = null,
                                          string &$warning = null): bool
    {
        if (!($isValid = VisitorModel::connect($username, $old))) {
            $warning = INVALID_CREDENTIALS;
        } elseif (!($isValid = ($areIdentical = $new == $newC) && Validation::validatePassword($new))) {
            $warning = $areIdentical ? PASSWORD_INSTRUCTIONS : PASSWORDS_NOT_IDENTICAL;
        } else {
            $isValid = (new UserGateway())->updatePassword(
                    $username,
                    password_hash($new, PASSWORD_BCRYPT)
            );
        }
        return $isValid;
    }
}
