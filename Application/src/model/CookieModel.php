<?php
/* *********************************************************************************************************************
 * Project name: Application
 * File name   : CookieModel
 * Author      : Damien Nguyen
 * Date        : Thursday, December 20 2018 
 * ********************************************************************************************************************/

/**
 * Class CookieModel.
 * This class manages cookie values getters and setters.
 */
class CookieModel
{
    /**
     * Gets the number of messages stored in a cookie.
     *
     * @return int the number of messages stored in a cookie
     */
    public static function getNumberOfMessages(): int
    {
        return Validation::validateInt($messagesCounter = $_COOKIE[MESSAGES_COUNTER])
                ? $messagesCounter
                : DEFAULT_INT_VALUE;
    }

    /**
     * Gets the page size stored in a cookie.
     *
     * @return int the page size stored in a cookie
     */
    public static function getPageSize(): int
    {
        return Validation::validateInt($pageSize = $_COOKIE[PAGE_SIZE]) ? $pageSize : DEFAULT_PAGE_SIZE;
    }

    /**
     * Increments the number of messages.
     */
    public static function incrementNumberOfMessages()
    {
        setcookie(
                MESSAGES_COUNTER,
                self::getNumberOfMessages() + 1,
                DEFAULT_COOKIE_TIME
        );
    }

    /**
     * Sets the given page size as the new associated cookie value.
     *
     * @param int $pageSize the given page size
     */
    public static function setPageSize(int $pageSize)
    {
        setcookie(PAGE_SIZE, $pageSize, DEFAULT_COOKIE_TIME);
    }
}
