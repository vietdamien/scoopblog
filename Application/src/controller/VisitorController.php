<?php
/* *********************************************************************************************************************
 * Project name: Application
 * File name   : VisitorController
 * Author      : Damien Nguyen
 * Date        : Friday, November 23 2018
 * ********************************************************************************************************************/

/**
 * Class VisitorController.
 * This class manages basic visitor actions.
 */
class VisitorController
{
    const DEFAULT_ACTION = 'displayPostsList';

    /**
     * VisitorController constructor.
     *
     * @param string|null $action the requested action
     */
    public function __construct(string $action = null)
    {
        global $view;
        $errors = [];

        if (empty($action)) {
            $action = self::DEFAULT_ACTION;
        }

        try {
            if (method_exists($this, $action)) {
                Validation::isStaticMethod($this, $action) ? self::$action() : $this->$action();
            } else {
                throw new Error('Visitor: invalid action.');
            }
        } catch (Error | Exception $e) {
            $errors[] = $e->getMessage();
            require ROOT . $view['error'];
        }
    }

    /**
     * Displays the connection form to allow the User to connect.
     *
     * @param string|null $success an additional success message as a positive feedback
     * @param string|null $warning an additional warning if the User failed to fill the fields with correct credentials
     *
     * @throws Exception if the display of the index failed
     */
    static function displayConnectionForm(string $success = null, string $warning = null)
    {
        global $view;
        if (!isset($_SESSION[ROLE])) {
            require ROOT . $view['connection-form'];
        } else {
            self::displayPostsList('You are already connected or trying to do something you cannot do.');
        }
    }

    /**
     * Displays the details of the Post that has the given ID.
     *
     * @param string|null $warning an additional warning if the User left one of the fields empty
     *
     * @throws Exception if the display failed
     */
    static function displayPostDetail(string $warning = null)
    {
        global $view;
        $author   = $_SESSION[USERNAME] ?? null;
        $comments = CommentModel::retrieve($id = Cleaner::cleanCounter($_REQUEST['id']));
        $post     = PostModel::retrievePost($id);
        require ROOT . $view['post-detail'];
    }

    /**
     * Displays a list of all the posts that correspond to the given requested page and the given key string.
     *
     * @param string|null $warning an additional warning if the User is already connected
     *
     * @throws Exception if the display failed
     */
    static function displayPostsList(string $warning = null)
    {
        global $view;
        $currentPage           = Cleaner::cleanPage(
                $numberOfPages = PostModel::getNumberOfPages(
                        $pageSize = CookieModel::getPageSize(),
                        $keyString = Cleaner::cleanString($_REQUEST['search'])
                ),
                $requestedPage = Cleaner::cleanPage($numberOfPages, $_REQUEST['page'])
        );
        $posts                 = PostModel::retrieve($requestedPage, $pageSize, $keyString,
                                                     Cleaner::cleanSearchType($_REQUEST['type']));
        $cookieCommentsCounter = CookieModel::getNumberOfMessages();
        $totalCommentsCounter  = CommentModel::count();
        require ROOT . $view['posts-list'];
    }

    /**
     * Gives the CommentModel the right information to add a new Comment in the database.
     *
     * @throws Exception if the addition failed
     */
    private function comment()
    {
        $id = Cleaner::cleanCounter($_REQUEST['id']);
        if (Validation::validateString(Cleaner::cleanString($comment = $_REQUEST['comment']))
            && CommentModel::add($comment, $id, Cleaner::cleanString(
                        $_SESSION[USERNAME] ?? $_REQUEST['author']
                ))) {
            CookieModel::incrementNumberOfMessages();
        } else {
            $warning = 'One of the fields is empty!';
        }
        self::displayPostDetail($warning ?? null);
    }

    /**
     * Gives the VisitorModel the right information to allow the User to connect.
     *
     * @throws Exception if the connection failed
     */
    private function connect()
    {
        if (VisitorModel::connect(Cleaner::cleanString($_REQUEST['username']),
                                  Cleaner::cleanString($_REQUEST['password']), $warning)) {
            header(HEADER_INDEX_LOCATION);
        } else {
            $this->displayConnectionForm(null, $warning);
        }
    }

    /**
     * Displays the password recovery form.
     *
     * @param string|null $warning an additional warning if the User failed to fill the email field correctly
     */
    private function displayPasswordRecoveryForm(string $warning = null)
    {
        global $view;
        require ROOT . $view['password-recovery-form'];
    }

    /**
     * Displays the registration form.
     */
    private function displayRegistrationForm()
    {
        global $view;
        require ROOT . $view['registration-form'];
    }

    /**
     * Displays the details of the User that has the given username.
     *
     * @throws Exception if the retrieval of the User failed
     */
    private function displayUserDetail()
    {
        global $view;
        $currentSessionUsername = $_SESSION[USERNAME];
        $user                   = VisitorModel::retrieveUser($author = Cleaner::cleanString($_REQUEST['username']));
        $numberOfComments       = CommentModel::count($author);
        require ROOT . $view['user-detail'];
    }

    /**
     * Gives the VisitorModel the right information to reset the password.
     *
     * @throws Exception if the password recovery failed
     */
    private function recoverPassword()
    {
        if (VisitorModel::recoverPassword($_REQUEST['email'], $warning)) {
            self::displayConnectionForm('Your password was successfully reset. You may now sign in.');
        } else {
            $this->displayPasswordRecoveryForm($warning ?? 'Please check your information again.');
        }
    }

    /**
     * Gives the VisitorModel the right information to register a new User in the database.
     * The validation will be done in the VisitorModel.
     *
     * @throws Exception if the registration failed
     */
    private function register()
    {
        global $view;
        if (!VisitorModel::register(Cleaner::cleanString($_REQUEST['username']), $_REQUEST['password'],
                                    $_REQUEST['passwordC'], $_REQUEST['email'], $warning, $success)) {
            require ROOT . $view['registration-form'];
        } else {
            require ROOT . $view['connection-form'];
        }
    }

    /**
     * Updates the page size.
     */
    private function updatePageSize()
    {
        CookieModel::setPageSize(Cleaner::cleanPageSize($_REQUEST['pageSize']));
        header(HEADER_INDEX_LOCATION);
    }
}
