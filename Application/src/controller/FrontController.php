<?php
/* *********************************************************************************************************************
 * Project name: Application
 * File name   : FrontController
 * Author      : Damien Nguyen
 * Date        : Tuesday, December 11 2018 
 * ********************************************************************************************************************/

/**
 * Class FrontController.
 * This class tests the requested action and calls the right controller.
 */
class FrontController
{
    /**
     * @var array the list of actions that can be done by role.
     */
    private $validActions = [
            ADMINISTRATOR => ['createPost', 'deleteComment', 'deletePosts', 'deleteUser', 'displayPostCreationForm',
                              'displayUsersList', 'updateRole'],
            MEMBER => ['disconnect', 'displayPasswordUpdateForm', 'updatePassword'],
            VISITOR => [null, 'connect', 'displayConnectionForm', 'displayPasswordRecoveryForm', 'displayPostDetail',
                        'displayPostsList', 'displayRegistrationForm', 'displayUserDetail', 'recoverPassword',
                        'register', 'comment', 'updatePageSize'],
    ];

    /**
     * FrontController constructor.
     */
    public function __construct()
    {
        global $isAdministrator, $view;
        $errors = [];

        try {
            if (!($role = $this->getRole($action = Cleaner::cleanString($_REQUEST['action'])))) {
                throw new Exception('This action matches none of the existing roles.');
            }

            if ((!($isAdministrator = AdministratorModel::isAdministrator()) && $role == ADMINISTRATOR)
                || (!MemberModel::isMember() && $role == MEMBER)) {
                VisitorController::displayConnectionForm(null, 'You must be signed in.');
            }

            $className = $role . CONTROLLER;
            new $className($action);
        } catch (Error | Exception $e) {
            $errors[] = $e->getMessage();
            require ROOT . $view['error'];
        }
    }

    /**
     * Gets from which sub-array the given action is from.
     *
     * @param string|null $action the given action
     *
     * @return bool|int|string the key of the sub-array that contains the given action, false otherwise
     */
    private function getRole(string $action = null)
    {
        foreach ($this->validActions as $key => $value) {
            if (in_array($action, $value)) {
                return $key;
            }
        }
        return false;
    }
}
