<?php
/* *********************************************************************************************************************
 * Project name: Application
 * File name   : MemberController
 * Author      : Damien Nguyen
 * Date        : Thursday, December 13 2018 
 * ********************************************************************************************************************/

/**
 * Class MemberController.
 * This class manages member actions.
 */
class MemberController
{
    /**
     * MemberController constructor.
     *
     * @param string $action the requested action
     */
    public function __construct(string $action)
    {
        global $view;
        $errors = [];

        try {
            if (method_exists($this, $action)) {
                $this->$action();
            } else {
                throw new Error('Member: invalid action.');
            }
        } catch (Error | Exception $e) {
            $errors[] = $e->getMessage();
            require ROOT . $view['error'];
        }
    }

    /**
     * Destroys the ongoing session to disconnect the User.
     *
     * @throws Exception if the display of the posts failed
     */
    private function disconnect()
    {
        MemberModel::disconnect();
        header(HEADER_INDEX_LOCATION);
    }

    /**
     * Displays the password change form to allow the User to update their password.
     *
     * @param string|null $warning an additional warning if the User failed to fill the fields with correct credentials
     */
    private function displayPasswordUpdateForm(string $warning = null)
    {
        global $view;
        require ROOT . $view['password-update-form'];
    }

    /**
     * Gives the MemberModel the right information to update the current user's password in the database.
     *
     * @throws Exception if the password update failed
     */
    private function updatePassword()
    {
        if (MemberModel::updatePassword(Cleaner::cleanString($_SESSION[USERNAME]),
                                        Cleaner::cleanString($_REQUEST['currentPassword']), $_REQUEST['newPassword'],
                                        $_REQUEST['newPasswordC'], $warning)) {
            MemberModel::disconnect();
            VisitorController::displayConnectionForm(
                    'You have successfully changed your password. You may now sign back in.'
            );
        } else {
            $this->displayPasswordUpdateForm($warning);
        }
    }
}
