<?php
/* *********************************************************************************************************************
 * Project name: Application
 * File name   : AdministratorController
 * Author      : Damien Nguyen
 * Date        : Friday, December 14 2018 
 * ********************************************************************************************************************/

/**
 * Class AdministratorController.
 * This class manages administrator actions.
 */
class AdministratorController
{
    /**
     * AdministratorController constructor.
     *
     * @param string $action the requested action
     */
    public function __construct(string $action)
    {
        global $view;
        $errors = [];

        try {
            if (method_exists($this, $action)) {
                $this->$action();
            } else {
                throw new Error('Administrator: invalid action.');
            }
        } catch (Error | Exception $e) {
            $errors[] = $e->getMessage();
            require ROOT . $view['error'];
        }
    }

    /**
     * Gives the PostModel the right information to add a new Post in the database.
     *
     * @throws Exception if the submission failed
     */
    private function createPost()
    {
        if (Validation::validateString($title = Cleaner::cleanString($_REQUEST['title']))
            && Validation::validateString(Cleaner::cleanString($content = $_REQUEST['content']))) {
            $_REQUEST['id'] = PostModel::add($title, $content, Cleaner::cleanString($_SESSION[USERNAME]));
            VisitorController::displayPostDetail();
        } else {
            $this->displayPostCreationForm('At least one of the fields is empty.');
        }
    }

    /**
     * Deletes a Comment.
     *
     * @throws Exception if the deletion failed
     */
    private function deleteComment()
    {
        VisitorController::displayPostDetail(
                Validation::validateInt(Cleaner::cleanInt($deleteID = $_REQUEST['deleteID']))
                && CommentModel::delete($deleteID)
                        ? null
                        : 'The comment ID is invalid.');
    }

    /**
     * Displays the post creation form.
     *
     * @param string|null $warning an additional warning if one of the fields was left empty
     */
    private function displayPostCreationForm(string $warning = null)
    {
        global $view;
        require ROOT . $view['post-creation-form'];
    }

    /**
     * Deletes a Post.
     *
     * @throws Exception if the deletion failed
     */
    private function deletePosts()
    {
        VisitorController::displayPostsList(
                (($id = $_REQUEST['id']) == null || Validation::validateInt($id))
                && PostModel::delete($id)
                        ? null
                        : 'The post ID is invalid.'
        );
    }

    /**
     * Deletes a User.
     *
     * @throws Exception if the deletion failed
     */
    private function deleteUser()
    {
        $this->displayUsersList(
                $isValid = AdministratorModel::delete(Cleaner::cleanString($_REQUEST['username']))
                        ? 'The deletion was successful.'
                        : null,
                $isValid
                        ? null
                        : 'The username is invalid.');
    }

    /**
     * Displays the users list.
     *
     * @param string|null $success an additional success message as a positive feedback
     * @param string|null $warning an additional warning if any operation on one of the users failed
     *
     * @throws Exception if the retrieval of the users failed
     */
    private function displayUsersList(string $success = null, string $warning = null)
    {
        global $view;
        $currentSessionUsername = $_SESSION[USERNAME];
        $users                  = AdministratorModel::retrieve(
                Validation::validateString($username = $_REQUEST['search'])
                        ? Cleaner::cleanString($username)
                        : null
        );
        require ROOT . $view['users-list'];
    }

    /**
     * Updates the role of a User.
     *
     * @throws Exception if the update failed
     */
    private function updateRole()
    {
        $this->displayUsersList(
                $isValid = AdministratorModel::updateRole(Cleaner::cleanString($_REQUEST['username']),
                                                          Cleaner::cleanString($_REQUEST['role']))
                        ? 'The update was successful.'
                        : null,
                $isValid
                        ? null
                        : 'The update failed.'
        );
    }
}
