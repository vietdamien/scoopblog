<?php
/* *********************************************************************************************************************
 * Project name: Application
 * FIle name   : index.php
 * Author      : Damien Nguyen
 * Date        : Tuesday, November 20 2018
 * ********************************************************************************************************************/

require_once __DIR__ . '/src/config/config.php';
require_once __DIR__ . '/src/config/Autoloader.php';

session_start();

Autoloader::load();

new FrontController();
